const {
  isSendingKeys,
  isExecutingJavascript
} = require('../lib/identify-command-from-req');

describe('Screenshot triggers', () => {
  describe('isSendingKeys', () => {
    it('should accept basic element id (from selenium-webdriver@3.5.0 and Docker image, selenium/standalone-chrome:3.4.0-francium)', () => {
      const result = isSendingKeys({
        method: 'POST',
        path: '/session/205d56a6-f4ff-40b0-80e2-670c0a818c76/element/0/value'
      }).result;

      expect(result).toBe(true);
    });

    it('should accept basic element id (from spec, https://w3c.github.io/webdriver/webdriver-spec.html#x11-elements)', () => {
      const result = isSendingKeys({
        method: 'POST',
        path: '/session/205d56a6-f4ff-40b0-80e2-670c0a818c76/element/element-6066-11e4-a52e-4f735466cecf/value'
      }).result;

      expect(result).toBe(true);
    });

    it('should work with complex element id (from selenium-webdriver@3.6.0 and chromedriver@2.35.0)', () => {
      const result = isSendingKeys({
        method: 'POST',
        path: '/session/e89d1b8305d3ad99a7aac892d0328cb8/element/0.8223660020227237-1/value'
      }).result;

      expect(result).toBe(true);
    });
  });

  describe('isExecutingJavascript', () => {
    it('should return true for async script execution', () => {
      const result = isExecutingJavascript({
        method: 'POST',
        path: '/session/e89d1b8305d3ad99a7aac892d0328cb8/execute/async/'
      }).result;

      expect(result).toBe(true);
    });

    it('should return true for sync script execution', () => {
      const result = isExecutingJavascript({
        method: 'POST',
        path: '/session/e89d1b8305d3ad99a7aac892d0328cb8/execute/sync'
      }).result;

      expect(result).toBe(true);
    });
  });
});
