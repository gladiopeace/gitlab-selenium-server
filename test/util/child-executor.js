var Promise = require('bluebird');
var spawn = require('child_process').spawn;

class ChildExecutor {
  constructor() {
    this.child = null;
    this.stdout = '';
    this.stderr = '';
  }

  exec(command, options) {
    var commandParts = command.split(' ');
    return new Promise((resolve, reject) => {
      // We use `child` so we can manually send a kill
      // ex.
      // var executor = new ChildExecutor();
      // executor.exec('SOMECOMMAND').then((resultInfo) => { console.log('resultInfo'); });
      // executor.child.kill('SIGINT');
      //
      // We have to use `spawn` instead of `exec` because you can't kill it
      //console.log('spawn', commandParts[0], commandParts.slice(1), options);
      this.child = spawn(commandParts[0], commandParts.slice(1), options);

      this.child.stdout.on('data', (data) => {
        //console.log('ce data', String(data));
        this.stdout += data;
      });

      this.child.stderr.on('data', (data) => {
        //console.log('ce err data', String(data));
        this.stderr += data;
      });

      this.child.on('close', () => {
        //console.log('ce close', this.stdout, '|', this.stderr);
        resolve({
          command: command,
          stdout: this.stdout,
          stderr: this.stderr
        });
      });

      this.child.on('error', (err) => {
        //console.log('ce error', this.stdout, '|', this.stderr);
        reject({
          command: command,
          stdout: this.stdout,
          stderr: this.stderr,
          error: err
        });
      });

      //this.child.stdout.pipe(process.stdout);
      //this.child.stderr.pipe(process.stderr);
    });
  }
}

module.exports = ChildExecutor;
