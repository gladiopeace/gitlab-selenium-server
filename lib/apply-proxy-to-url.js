const urlJoin = require('url-join');
const url = require('url');

function applyProxyToUrl(targetEndpoint, urlToProxy) {
  const parsedUrlToProxy = url.parse(urlToProxy);
  const parsedTargetUrl = url.parse(targetEndpoint);

  const resultantTargetUrl = url.format(Object.assign({}, parsedUrlToProxy, {
    protocol: parsedTargetUrl.protocol,
    slashes: parsedTargetUrl.slashses,
    auth: parsedTargetUrl.auth,
    host: parsedTargetUrl.host,
    port: parsedTargetUrl.port,
    hostname: parsedTargetUrl.hostname,
    // We check for slash to get around this odd behavior
    // `urlJoin('/', '/', '/session')` -> `'//session'` <---- unexpected (expected `'/session'`)
    // `urlJoin('/', '/wd/hub', '/session')` -> `'/wd/hub/session'`
    // `urlJoin('/', '/wd/hub/', '/session')` -> `'/wd/hub/session'`
    pathname: urlJoin('/', (parsedTargetUrl.pathname !== '/' ? parsedTargetUrl.pathname : ''), parsedUrlToProxy.path),
  }));

  return resultantTargetUrl;
}

module.exports = applyProxyToUrl;
