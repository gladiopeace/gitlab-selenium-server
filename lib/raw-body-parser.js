const bodyParser = require('body-parser');

// We use the `bodyParser.raw` to get things fully intact in `req.body`
// This can result in `req.body` being a Buffer or Object
const rawBodyParser = bodyParser.raw({
  type: () => {
    return true;
  }
});

module.exports = rawBodyParser;
